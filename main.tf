terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "3.58.0"
    }
  }
}

# azure 인증 정보
provider "azurerm" {
  features {}

  subscription_id   = var.subscription_id
  tenant_id         = var.tenant_id
  client_id         = var.client_id
  client_secret     = var.client_secret
}

# resource group 생성
resource "azurerm_resource_group" "rg" {
  name     = "${var.prefix}-azure-network-rg"
  location = var.resource_group_location
}

# virtual network 생성
resource "azurerm_virtual_network" "vnet" {
  name                = "${var.prefix}-virtual-network"
  address_space       = var.vnet_range
  dns_servers         = var.dns_servers
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
}

# subnet 생성
resource "azurerm_subnet" "subnet_1" {
  name                 = "${var.prefix}-subnet_1"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = var.subnet_1_range
}

resource "azurerm_subnet" "subnet_2" {
  name                 = "${var.prefix}-subnet_2"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = var.subnet_2_range
}

resource "azurerm_subnet" "subnet_3" {
  name                 = "${var.prefix}-subnet_3"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = var.subnet_3_range
}

# security group 생성
resource "azurerm_network_security_group" "InOut" {
  name                = "${var.prefix}-network_security-group"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  # Inbound 규칙
  security_rule {
    name                       = "Inbound"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
    /* 현재 접속하고 있는 Client의 IP를 받아와야 할 경우 사용
    source_address_prefix      = var.client_address
    destination_address_prefix = var.client_address
    */
  }
  # Outbound 규칙
  security_rule {
    name                       = "Outbound"
    priority                   = 101
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

/* ssh 접속으로 진행할 경우 사용
resource "azurerm_ssh_public_key" "example" {
  name                = "${var.prefix}-key"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  public_key          = file("~/.ssh/id_rsa.pub")
}
*/

# subnet과 security group 연결
resource "azurerm_subnet_network_security_group_association" "InOut_association_1" {
  subnet_id                 = azurerm_subnet.subnet_1.id
  network_security_group_id = azurerm_network_security_group.InOut.id
}

resource "azurerm_subnet_network_security_group_association" "InOut_association_2" {
  subnet_id                 = azurerm_subnet.subnet_2.id
  network_security_group_id = azurerm_network_security_group.InOut.id
}

resource "azurerm_subnet_network_security_group_association" "InOut_association_3" {
  subnet_id                 = azurerm_subnet.subnet_3.id
  network_security_group_id = azurerm_network_security_group.InOut.id
}
