variable "subscription_id" {
}

variable "tenant_id" {
}

variable "client_id" {
}

variable "client_secret" {
}

variable "resource_group_location" {
  default     = "koreacentral"
}

variable "dns_servers" {
  type        = list(string)
  default     = ["8.8.8.8"]
}

variable "vnet_range" {
  type        = list(string)
  default     = ["10.0.0.0/8"]
}

variable "subnet_1_range" {
  type        = list(string)
  default     = ["10.1.0.0/16"]
}

variable "subnet_2_range" {
  type        = list(string)
  default     = ["10.2.0.0/16"]
}

variable "subnet_3_range" {
  type        = list(string)
  default     = ["10.3.0.0/16"]
}

variable "prefix" {
  type        = string
  default     = "samsung-poc"
}

/* 현재 접속하고 있는 Client의 IP를 받아와야 할 경우 사용
variable "client_address" {
  type        = string
}
*/
