output "location" {
  value = azurerm_resource_group.rg.location
}

output "rg_name" {
  value = azurerm_resource_group.rg.name
}

output "network_name" {
  value = azurerm_virtual_network.vnet.name
}

output "subnet_id" {
  value = [azurerm_subnet.subnet_1.id, azurerm_subnet.subnet_2.id, azurerm_subnet.subnet_3.id]
}
